# Requirements:
 *  REQ01: - Cell: Food can be added to the Cell
 *  REQ02: - Cell: We can track how many food remaining in Cell
 *  REQ03: - Animal: Every animal can eat, sleep, move to find food or do nothing/something
 *  REQ04: - Animal: Every animal will have own behaviour, which actions to do depending on internal state.
 *  REQ05: - Animal: An animal has some probability percent to find food
 *  REQ06: - Animal: Different animals will eat different amount of food
 *  REQ07: - Animal: Every animal need to have at least 2 parameters to track sleep and hungry. You can add more parameters to emulate behaviour
 *  REQ08: - Animal: Different animals will have different internal parameters, thresholds etc
 *  REQ09: - Animal: An animal active should increase some internal parameter which will lead (after some threshold reached) to sleep
 *  REQ10: - Animal: If animal will be hungry (below some threshold) more than 24h it will leave Zoo.
 *  REQ11: - Animal: Every hour animal need to decide what to do depending on internal state
 *  REQ12: - Animal: If you have more than XX amount of food in the End of the Day - all animals of the Cell will leave Zoo

# Bonus:
 *  REQ13: - Animals can eat each other
package com.j2core.maxgls.week4.zoo.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link Cage} interface for internal needs (that's why the class access level - default/package)
 *
 * @author maxgls
 */
class CageImpl implements Cage {

    private final String name;
    private int foodCount;
    private final List<BaseAnimal> animals = new ArrayList<>();

    CageImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getFoodCount() {
        return this.foodCount;
    }

    @Override
    public void addFood(int foodCount) {
        this.foodCount += foodCount;
    }

    @Override
    public void removeFood(int foodCount) {
        this.foodCount = this.foodCount < foodCount ? 0 : this.foodCount - foodCount;
    }

    @Override
    public int getFoodNormalPortion() {
        int totalFoodNormalPortion = 0;
        for (BaseAnimal animal : this.animals) {
            totalFoodNormalPortion += animal.getFoodNormalPortion();
        }
        return totalFoodNormalPortion;
    }

    @Override
    public Collection<BaseAnimal> getAnimals() {
        return this.animals;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
package com.j2core.maxgls.week4.zoo.base;

import com.j2core.maxgls.week4.zoo.utils.Percent;
import org.apache.log4j.Logger;

import java.util.UUID;

/**
 * Represents a base entity of animal that combines animal behavior and data.
 *
 * @author maxgls
 */
public abstract class BaseAnimal extends AnimalBehavior {

    private static final Logger logger = Logger.getLogger(BaseAnimal.class);

    private static final int MIN_FOOD_NORMAL_PORTION = 1;
    private static final int DEFAULT_FIND_FOOD_PERCENT = 70;

    private final String id;
    private final int foodNormalPortion;
    private final int findFoodPercent;
    private boolean isSleep;
    private Percent energyPercent;
    private Percent fullnessPercent;
    private Percent livenessPercent;

    public BaseAnimal(int foodNormalPortion) {
        this(foodNormalPortion, DEFAULT_FIND_FOOD_PERCENT);
    }

    public BaseAnimal(int foodNormalPortion, int findFoodPercent) {
        this.id = this.getClass().getSimpleName() + "-" + UUID.randomUUID().toString();
        this.foodNormalPortion = foodNormalPortion <= 0 ? MIN_FOOD_NORMAL_PORTION : foodNormalPortion;
        this.findFoodPercent = new Percent(findFoodPercent).getValue();
        this.energyPercent = new Percent(Percent.MAX_VALUE);
        this.fullnessPercent = new Percent(Percent.MAX_VALUE);
        this.livenessPercent = new Percent(Percent.MAX_VALUE);
        logger.debug(this + " - [constructor] foodNormalPortion='" + this.foodNormalPortion + "'");
    }

    @Override
    public final String getId() {
        return this.id;
    }

    @Override
    public final int getFoodNormalPortion() {
        return this.foodNormalPortion;
    }

    @Override
    public int getRequiredFoodCount() {
        return (int)Math.round(this.getFoodNormalPortion() * this.getHungryPercent() * 0.01 );
    }

    @Override
    public final int getFindFoodPercent() {
        return this.findFoodPercent;
    }

    @Override
    public Percent getEnergy() {
        return this.energyPercent;
    }

    @Override
    public boolean isTired() {
        return this.energyPercent.getValue() == Percent.MIN_VALUE;
    }

    @Override
    public boolean isSleep() {
        return this.isSleep;
    }

    @Override
    public void setSleep(boolean isSleep) {
        this.isSleep = isSleep;
    }

    @Override
    public Percent getFullness() {
        return this.fullnessPercent;
    }

    @Override
    public void increaseFullness(int foodCount) {
        final int requiredFoodCount = this.getRequiredFoodCount();
        final String logMessageTemplate = "%1$s - [increaseFullness] Amount of food is %2$s. (fullnessPercent='%3$s')";

        if (foodCount >= requiredFoodCount) {
            this.fullnessPercent.setValue(Percent.MAX_VALUE);
            logger.debug(String.format(logMessageTemplate, this, "enough", this.fullnessPercent));
        }
        else {
            final int percentValue = foodCount * Percent.MAX_VALUE / requiredFoodCount;
            this.fullnessPercent.increase(percentValue);
            logger.debug(String.format(logMessageTemplate, this, "NOT enough", this.fullnessPercent));
        }
    }

    @Override
    public boolean isHungry() {
        return this.fullnessPercent.getValue() == Percent.MIN_VALUE;
    }

    @Override
    public Percent getLiveness() {
        return this.livenessPercent;
    }

    @Override
    public boolean isDead() {
        return this.livenessPercent.getValue() == Percent.MIN_VALUE;
    }

    private int getHungryPercent() {
        return Percent.MAX_VALUE - this.getFullness().getValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof BaseAnimal)) {
            return false;
        }
        final BaseAnimal other = (BaseAnimal) obj;
        return this.getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    @Override
    public String toString() {
        return this.getId();
    }
}
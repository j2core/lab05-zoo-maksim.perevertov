package com.j2core.maxgls.week4.zoo.behavior;

import com.j2core.maxgls.week4.zoo.base.Zoo;

/**
 * Represents a zoo time behavior abstraction for {@link ZooTimeEmulator}
 *
 * @author maxgls
 */
public interface TimeBehavior {

    void act(Zoo zoo, TimeEvent timeEvent);
}
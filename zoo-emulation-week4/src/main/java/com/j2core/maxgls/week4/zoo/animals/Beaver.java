package com.j2core.maxgls.week4.zoo.animals;

import com.j2core.maxgls.week4.zoo.base.BaseAnimal;
import com.j2core.maxgls.week4.zoo.base.Cage;
import org.apache.log4j.Logger;

public class Beaver extends BaseAnimal {

    private static final Logger logger = Logger.getLogger(Beaver.class);

    private static final int NORMAL_FOOD_PORTION = 6;
    private static final int DEFAULT_MAX_DAM_COUNT = 2;

    private static final int SWIM_ENERGY_COST = 8;
    private static final int SWIM_FULLNESS_COST = 20;
    private static final int BUILD_DAM_ENERGY_COST = 10;
    private static final int BUILD_DAM_FULLNESS_COST = 25;
    private static final int WALK_ENERGY_COST = 6;
    private static final int WALK_FULLNESS_COST = 15;

    private static final String MSG_STRING_FORMAT = "%1$s - [%2$s] fullnessPercent:%3$s, energyPercent:%4$s";
    private final int maxDamCount;
    private int damCounter;

    public Beaver() {
        super(NORMAL_FOOD_PORTION);
        this.maxDamCount = DEFAULT_MAX_DAM_COUNT;
    }

    public Beaver(int maxDamCount) {
        super(NORMAL_FOOD_PORTION);
        this.maxDamCount = maxDamCount;
    }

    @Override
    protected void doSomething(Cage cage) {
        // Beaver builds dams until max count is reached
        if (!this.isDamEnough()) {
            this.buildDam();
            return;
        }
        // Beaver does other action if it does not have to build dams
        if (super.randMoveAction.nextBoolean()) {
            this.swim();
        }
        else {
            this.walk();
        }
    }

    // Custom Beaver's parameters (data):
    public boolean isDamEnough() {
        return this.damCounter >= this.maxDamCount;
    }

    // Custom Beaver's actions:
    private void swim() {
        this.getEnergy().decrease(SWIM_ENERGY_COST);
        this.getFullness().decrease(SWIM_FULLNESS_COST);
        logger.info(String.format(MSG_STRING_FORMAT, this, "swim", this.getFullness(), this.getEnergy()));
    }

    private void buildDam() {
        this.getEnergy().decrease(BUILD_DAM_ENERGY_COST);
        this.getFullness().decrease(BUILD_DAM_FULLNESS_COST);
        this.damCounter++;
        logger.info(String.format(MSG_STRING_FORMAT, this, "buildDam", this.getFullness(), this.getEnergy()) + ", damCounter:" + this.damCounter);
    }

    private void walk() {
        this.getEnergy().decrease(WALK_ENERGY_COST);
        this.getFullness().decrease(WALK_FULLNESS_COST);
        logger.info(String.format(MSG_STRING_FORMAT, this, "walk", this.getFullness(), this.getEnergy()));
    }
}
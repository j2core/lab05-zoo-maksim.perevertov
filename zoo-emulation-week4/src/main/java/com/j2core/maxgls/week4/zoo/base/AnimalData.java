package com.j2core.maxgls.week4.zoo.base;

import com.j2core.maxgls.week4.zoo.utils.Percent;

/**
 * Represents an entry to keep animal data separately from a behavior
 *
 * @author maxgls
 */
public interface AnimalData {

    /**
     * Returns the unique identifier for this animal
     *
     * @return the unique identifier
     */
    String getId();

    /**
     * Returns the normal food portion for this animal
     * REQ05 - MainZooEmulator
     *
     * @return the normal food portion for this animal
     */
    int getFoodNormalPortion();

    /**
     * Returns the required amount of food to this animal become fed.
     * The required amount of food depends on {@link #getFullness()} and {@link #getFoodNormalPortion()}
     *
     * @return the amount of food to be full
     */
    int getRequiredFoodCount();

    /**
     * Returns the find food percent for this animal
     * REQ06 - MainZooEmulator
     *
     * @return the find food percent for this animal
     */
    int getFindFoodPercent();

    /**
     * Returns the energy percent of this animal
     * REQ09 - MainZooEmulator
     *
     * @return the energy percent of this animal
     */
    Percent getEnergy();

    /**
     * Returns <tt>true</tt> if this animal is tired
     * This parameters depends on {@link #getEnergy()}
     * REQ07 - MainZooEmulator
     *
     * @return <tt>true</tt> if this animal is tired
     */
    boolean isTired();

    /**
     * Returns <tt>true</tt> if this animal sleeps
     * This parameters can depend on {@link #getEnergy()}
     * REQ07 - MainZooEmulator
     *
     * @return <tt>true</tt> if this animal sleeps
     */
    boolean isSleep();

    /**
     * Sets the value of {@link #isSleep()}
     *
     * @param isSleep the value of {@link #isSleep()}
     */
    void setSleep(boolean isSleep);

    /**
     * Returns the fullness percent of this animal
     *
     * @return the fullness percent of this animal
     */
    Percent getFullness();

    /**
     * Increases the fullness level of this animal depends on the specified food count and {@link #getFoodNormalPortion()}
     *
     * @param foodCount the available food count
     */
    void increaseFullness(int foodCount);

    /**
     * Returns <tt>true</tt> if this animal is hungry
     * REQ07- MainZooEmulator
     *
     * @return <tt>true</tt> if this animal is hungry
     */
    boolean isHungry();

    /**
     * Returns the liveness percent of this animal
     *
     * @return the liveness percent of this animal
     */
    Percent getLiveness();

    /**
     * Returns <tt>true</tt> if this animal is hungry
     * REQ07 - MainZooEmulator
     *
     * @return <tt>true</tt> if this animal is hungry
     */
    boolean isDead();
}
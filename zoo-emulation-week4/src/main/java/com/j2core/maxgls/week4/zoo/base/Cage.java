package com.j2core.maxgls.week4.zoo.base;

import com.j2core.maxgls.week4.zoo.MainZooEmulator;

import java.util.Collection;

/**
 * Represents interface for base cage of zoo
 *
 * @author maxgls
 */
public interface Cage {

    /**
     * Returns the unique name for cage in the zoo
     *
     * @return the unique name for cage in the zoo
     */
    String getName();

    /**
     * Returns the amount of food in the cage
     * REQ02 - {@link MainZooEmulator}
     *
     * @return the amount of food in the cage
     */
    int getFoodCount();

    /**
     * Adds food to this cage
     * REQ01 - {@link MainZooEmulator}
     *
     * @param foodCount the food count to be added to this cage
     */
    void addFood(int foodCount);

    /**
     * Decreases the amount of food in the cage. The amount of food can not be less than 0.
     *
     * An animal uses this method to eat food.
     * Worker uses this method to clean cage
     *
     * @param foodCount the amount of food to decrease
     */
    void removeFood(int foodCount);

    /**
     * Returns the total normal food portion for animals in this case
     *
     * @return the total normal food portion for animals in this case
     */
    int getFoodNormalPortion();

    /**
     * Returns a collection to iterate over the animals in this cage
     *
     * @return a <tt>Collection</tt> to iterate over the animals in this cage
     */
    Collection<BaseAnimal> getAnimals();
}
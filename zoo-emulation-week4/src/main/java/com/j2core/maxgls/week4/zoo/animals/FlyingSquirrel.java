package com.j2core.maxgls.week4.zoo.animals;

import com.j2core.maxgls.week4.zoo.base.Cage;
import org.apache.log4j.Logger;

public class FlyingSquirrel extends Squirrel {

    private static final Logger logger = Logger.getLogger(FlyingSquirrel.class);

    private static final int NORMAL_FOOD_PORTION = 3;
    private static final int PERCENT_TO_FIND_FOOD = 80;

    private static final int FLY_ENERGY_COST = 8;
    private static final int FLY_FULLNESS_COST = 15;

    public FlyingSquirrel() {
        super(NORMAL_FOOD_PORTION, PERCENT_TO_FIND_FOOD);
    }

    @Override
    protected void doSomething(Cage cage) {
        if (super.randMoveAction.nextBoolean()) {
            this.fly();
        }
        else {
            super.doSomething(cage);
        }
    }

    // Custom FlyingSquirrel's actions:
    private void fly() {
        this.getEnergy().decrease(FLY_ENERGY_COST);
        this.getFullness().decrease(FLY_FULLNESS_COST);
        logger.info(this + " - [fly] fullnessPercent:" + this.getFullness() + ", energyPercent:" + this.getEnergy());
    }
}
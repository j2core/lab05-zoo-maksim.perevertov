package com.j2core.maxgls.week4.zoo.behavior;

import com.j2core.maxgls.week4.zoo.base.Cage;
import com.j2core.maxgls.week4.zoo.base.Zoo;
import org.apache.log4j.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Represents a time logic/behavior for an automatic cage service system (ACSS) in the zoo
 *
 * @author maxgls
 */
public class AcssTimeBehavior implements TimeBehavior {

    private static final Logger logger = Logger.getLogger(AcssTimeBehavior.class);

    @Override
    public void act(Zoo zoo, TimeEvent timeEvent) {
        if (zoo == null) {
            throw new IllegalArgumentException("The parameter 'zoo' is null");
        }
        switch (timeEvent) {
            case Hour:
                break;
            case StartDay:
                this.actStartDay(zoo);
                break;
            case EndDay:
                this.actEndDay(zoo);
                break;
            default:
                throw new NotImplementedException();
        }
    }

    private void actStartDay(Zoo zoo) {
        for (Cage cage : zoo.getCages()) {
            this.feedAnimals(zoo, cage);
        }
    }

    private void actEndDay(Zoo zoo) {
        for (Cage cage : zoo.getCages()) {
            // Clean cage (remove spoiled food)
            final int foodCount = cage.getFoodCount();
            cage.removeFood(foodCount);
            logger.info(cage + " - [actEndDay] The cage has been cleaned (" + foodCount + ")");
            // Add fresh food
            feedAnimals(zoo, cage);
        }
    }

    private void feedAnimals(Zoo zoo, Cage cage) {
        final int cageFoodCount = cage.getFoodCount();
        final int totalFoodCount = zoo.getTotalFoodCount();
        int requiredCageFoodCount = cage.getFoodNormalPortion();
        // Calculates available amount of food to feed animals
        if (requiredCageFoodCount > totalFoodCount) {
            requiredCageFoodCount = totalFoodCount;
        }
        // Add food to cage and decrease total food count in zoo
        final int foodCountToAdd = requiredCageFoodCount - cageFoodCount;
        if (cageFoodCount < requiredCageFoodCount) {
            zoo.decreaseTotalFoodCount(foodCountToAdd);
            cage.addFood(foodCountToAdd);
            logger.info(cage + " - [feedAnimals] Food has been added:" + foodCountToAdd);
        }
    }
}
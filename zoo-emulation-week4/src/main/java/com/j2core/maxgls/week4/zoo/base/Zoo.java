package com.j2core.maxgls.week4.zoo.base;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Zoo {

    private int totalFoodCount;
    private final Map<String, Cage> cages = new HashMap<>();

    public Zoo(int totalFoodCount) {
        this.totalFoodCount = totalFoodCount;
    }

    public int getTotalFoodCount() {
        return this.totalFoodCount;
    }

    public void decreaseTotalFoodCount(int foodCount) {
        this.totalFoodCount = this.totalFoodCount < foodCount ? 0 : this.totalFoodCount - foodCount;
    }

    public void addAnimal(String cageName, Iterable<BaseAnimal> animals) {
        final Cage cage = this.getCageByName(cageName);
        for (BaseAnimal animal : animals) {
            cage.getAnimals().add(animal);
        }
    }

    public void addAnimal(String cageName, BaseAnimal... animals) {
        this.addAnimal(cageName, Arrays.asList(animals));
    }

    public int getAnimalsCount() {
        int animalsCount = 0;
        for (Cage cage : this.cages.values()) {
            animalsCount += cage.getAnimals().size();
        }
        return animalsCount;
    }

    public Collection<Cage> getCages() {
        return this.cages.values();
    }

    private Cage getCageByName(String cageName) {
        if (!cages.containsKey(cageName)) {
            cages.put(cageName, new CageImpl(cageName));
        }
        return cages.get(cageName);
    }
}
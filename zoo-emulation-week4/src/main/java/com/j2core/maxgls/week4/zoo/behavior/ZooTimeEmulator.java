package com.j2core.maxgls.week4.zoo.behavior;

import com.j2core.maxgls.week4.zoo.base.Zoo;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents zoo emulator to run process of live emulation in time
 *
 * @author maxgls
 */
public class ZooTimeEmulator {

    private static final Logger logger = Logger.getLogger(ZooTimeEmulator.class);

    private static final int HOURS_IN_DAY = 24;

    private final Zoo zoo;
    private int startDayHour;
    private int endDayHour;
    private final List<TimeBehavior> timeBehaviors = new ArrayList<TimeBehavior>();

    public ZooTimeEmulator(Zoo zoo, TimeBehavior... behaviors) {
        this.zoo = zoo;
        if (behaviors != null) {
            this.timeBehaviors.addAll(Arrays.asList(behaviors));
        }
    }

    public void startEmulation(int daysCount, int startDayHour, int endDayHour) {
        this.startDayHour = startDayHour;
        this.endDayHour = endDayHour;
        final int totalHours = daysCount * HOURS_IN_DAY;

        for (int i = startDayHour, currentHourInDay = startDayHour; i < totalHours; i++, currentHourInDay++) {
            // Reset 'currentHourInDay' every 24h
            if (HOURS_IN_DAY == currentHourInDay) {
                currentHourInDay = 0;
            }

            final int animalsCount = this.zoo.getAnimalsCount();
            logger.info(String.format("*** time hour: %1$s, day number: %2$s, food count: %3$s, count of animals: %4$s",
                    currentHourInDay, i / HOURS_IN_DAY, this.zoo.getTotalFoodCount(), animalsCount));
            if (animalsCount == 0) {
                logger.info("*** Zoo emulation has been stopped due to there are no animals in the zoo.");
                return;
            }

            // Performs time behavior logic for Zoo
            final TimeEvent timeEvent = getTimeEventByDayHour(currentHourInDay);
            for (TimeBehavior timeBehavior : this.timeBehaviors) {
                timeBehavior.act(this.zoo, timeEvent);
            }
        }
    }

    private TimeEvent getTimeEventByDayHour(int hourInDay) {
        if (hourInDay == this.startDayHour) {
            return TimeEvent.StartDay;
        }
        if (hourInDay == this.endDayHour) {
            return TimeEvent.EndDay;
        }
        return TimeEvent.Hour;
    }
}
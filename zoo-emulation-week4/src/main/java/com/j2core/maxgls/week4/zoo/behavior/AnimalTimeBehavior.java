package com.j2core.maxgls.week4.zoo.behavior;

import com.j2core.maxgls.week4.zoo.base.BaseAnimal;
import com.j2core.maxgls.week4.zoo.base.Cage;
import com.j2core.maxgls.week4.zoo.utils.Percent;
import com.j2core.maxgls.week4.zoo.base.Zoo;
import org.apache.log4j.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Iterator;

/**
 * Represents an animal time logic/behavior in the zoo
 *
 * @author maxgls
 */
public class AnimalTimeBehavior implements TimeBehavior {

    private static final Logger logger = Logger.getLogger(AnimalTimeBehavior.class);

    private static final int MAX_HUNGRY_HOURS = 24;

    @Override
    public void act(Zoo zoo, TimeEvent timeEvent) {
        if (zoo == null) {
            throw new IllegalArgumentException("The parameter 'zoo' is null");
        }
        for (Cage cage : zoo.getCages()) {
            switch (timeEvent) {
                case Hour:
                    this.actHour(cage);
                    break;
                case StartDay:
                    break;
                case EndDay:
                    this.actEndDay(cage);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }

    private void actHour(Cage cage) {
        for (BaseAnimal animal : cage.getAnimals()) {
            if (animal.isDead()) {
                continue;
            }
            animal.act(cage);
            // REQ10: If animal will be hungry more than 24h it will leave zoo/dead
            if (animal.isHungry()) {
                animal.getLiveness().decrease(Percent.MAX_VALUE / MAX_HUNGRY_HOURS);
                logger.debug(animal + " - [actHour] Animal is hungry, livenessPercent:" + animal.getLiveness());
            }
            else {
                animal.getLiveness().setValue(Percent.MAX_VALUE);
            }
        }
        // Remove dead animals from the cage
        final Iterator<BaseAnimal> animalIterator = cage.getAnimals().iterator();
        while (animalIterator.hasNext()) {
            final BaseAnimal animal = animalIterator.next();
            if (animal.isDead()){
                animalIterator.remove();
                logger.warn(animal + " - [actHour] Animal is dead and has been removed from cage");
            }
        }
    }

    private void actEndDay(Cage cage) {
        // REQ12: If you there is more than XX amount of food in the End of the Day - all animals of the Cell will leave zoo/dead
        if (cage.getFoodCount() > cage.getFoodNormalPortion()) {
            Iterator<BaseAnimal> animalIterator = cage.getAnimals().iterator();
            while (animalIterator.hasNext()) {
                final BaseAnimal animal = animalIterator.next();
                animalIterator.remove();
                logger.warn(animal + " - [actEndDay] Animal has been removed from the cage. Amount of food:" + cage.getFoodCount());
            }
        }
    }
}
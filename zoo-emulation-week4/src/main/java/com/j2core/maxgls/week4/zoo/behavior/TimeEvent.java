package com.j2core.maxgls.week4.zoo.behavior;

public enum TimeEvent {
    Hour,
    StartDay,
    EndDay
}
package com.j2core.maxgls.week4.zoo.animals;

import com.j2core.maxgls.week4.zoo.base.BaseAnimal;
import com.j2core.maxgls.week4.zoo.base.Cage;
import com.j2core.maxgls.week4.zoo.utils.Percent;
import org.apache.log4j.Logger;

import java.util.Collection;

/**
 * Fossa is predator. If there are no food in cage, Fossa can eat other animal.
 *
 * @author maxgls
 */
public class Fossa extends BaseAnimal {

    private static final Logger logger = Logger.getLogger(Fossa.class);

    private static final int NORMAL_FOOD_PORTION = 8;
    private static final int DEFAULT_MAX_HOLE_COUNT = 5;

    private static final int CLIMB_TREE_ENERGY_COST = 8;
    private static final int CLIMB_TREE_FULLNESS_COST = 20;
    private static final int PROWL_ENERGY_COST = 6;
    private static final int PROWL_FULLNESS_COST = 15;
    private static final int DIGGING_HOLE_ENERGY_COST = 10;
    private static final int DIGGING_HOLE_FULLNESS_COST = 25;

    private static final String MSG_STRING_FORMAT = "%1$s - [%2$s] fullnessPercent:%3$s, energyPercent:%4$s";
    private final int maxHoleCount;
    private int holeCounter;

    public Fossa() {
        super(NORMAL_FOOD_PORTION);
        this.maxHoleCount = DEFAULT_MAX_HOLE_COUNT;
    }

    public Fossa(int maxHoleCount) {
        super(NORMAL_FOOD_PORTION);
        this.maxHoleCount = maxHoleCount;
    }

    @Override
    public void act(Cage cage) {
        super.act(cage);

        if (!this.isDead() && this.isHungry()) {
            final Collection<BaseAnimal> animals = cage.getAnimals();
            for (BaseAnimal animal : animals) {
                if (!animal.isDead() &&
                        !this.getClass().isInstance(animal)) {
                    this.eatAnimal(animal);
                    break;
                }
            }
        }
    }

    @Override
    protected void doSomething(Cage cage) {
        if (!this.isHoleEnough()) {
            if (super.randMoveAction.nextBoolean()) {
                this.diggingHole();
            }
            else {
                this.climbTree();
            }
            return;
        }
        if (super.randMoveAction.nextBoolean()) {
            this.prowl();
        }
        else {
            this.climbTree();
        }
    }

    // Custom Fossa's parameters (data):
    public boolean isHoleEnough() {
        return this.holeCounter >= this.maxHoleCount;
    }

    // Custom Fossa's actions:
    private void eatAnimal(final BaseAnimal animal) {
        animal.getLiveness().setValue(Percent.MIN_VALUE);
        this.getFullness().setValue(Percent.MAX_VALUE);
        logger.info("The animal '" + animal.getId() + "' has been eaten by '" + this.getId()+ "'");
        logger.info(String.format(MSG_STRING_FORMAT, this, "eatAnimal", this.getFullness(), this.getEnergy()));
    }

    private void climbTree()  {
        this.getEnergy().decrease(CLIMB_TREE_ENERGY_COST);
        this.getFullness().decrease(CLIMB_TREE_FULLNESS_COST);
        logger.info(String.format(MSG_STRING_FORMAT, this, "climbTree", this.getFullness(), this.getEnergy()));
    }

    private void prowl() {
        this.getEnergy().decrease(PROWL_ENERGY_COST);
        this.getFullness().decrease(PROWL_FULLNESS_COST);
        logger.info(String.format(MSG_STRING_FORMAT, this, "prowl", this.getFullness(), this.getEnergy()));
    }

    private void diggingHole() {
        this.getEnergy().decrease(DIGGING_HOLE_ENERGY_COST);
        this.getFullness().decrease(DIGGING_HOLE_FULLNESS_COST);
        this.holeCounter++;
        logger.info(String.format(MSG_STRING_FORMAT, this, "diggingHole", this.getFullness(), this.getEnergy()) + ", holeCounter:" + this.holeCounter);
    }
}
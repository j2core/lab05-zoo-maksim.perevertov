package com.j2core.maxgls.week4.zoo;

import com.j2core.maxgls.week4.zoo.animals.Beaver;
import com.j2core.maxgls.week4.zoo.animals.FlyingSquirrel;
import com.j2core.maxgls.week4.zoo.animals.Fossa;
import com.j2core.maxgls.week4.zoo.animals.Squirrel;
import com.j2core.maxgls.week4.zoo.base.Zoo;
import com.j2core.maxgls.week4.zoo.behavior.AnimalTimeBehavior;
import com.j2core.maxgls.week4.zoo.behavior.ZooTimeEmulator;
import com.j2core.maxgls.week4.zoo.behavior.AcssTimeBehavior;

/**
 * Entry point for Zoo emulation
 *
 * Requirements:
 *  REQ01: - Cell: Food can be added to the Cell (Cage)
 *  REQ02: - Cell: We can track how many food remaining in Cell (Cage)
 *  REQ03: - Animal: Every animal can eat, sleep, move to find food or do nothing/something (AnimalBehavior)
 *  REQ04: - Animal: Every animal will have own behaviour, which actions to do depending on internal state. (AnimalBehavior)
 *  REQ05: - Animal: An animal has some probability percent to find food (AnimalData)
 *  REQ06: - Animal: Different animals will eat different amount of food (AnimalData)
 *  REQ07: - Animal: Every animal need to have at least 2 parameters to track sleep and hungry. You can add more parameters to emulate behaviour (AnimalData)
 *  REQ08: - Animal: Different animals will have different internal parameters, thresholds etc
 *  REQ09: - Animal: An animal active should increase some internal parameter which will lead (after some threshold reached) to sleep (AnimalData)
 *  REQ10: - Animal: If animal will be hungry (below some threshold) more than 24h it will leave Zoo. (AnimalTimeBehavior)
 *  REQ11: - Animal: Every hour animal need to decide what to do depending on internal state
 *  REQ12: - Animal: If you have more than XX amount of food in the End of the Day - all animals of the Cell will leave Zoo (AnimalTimeBehavior)
 *
 * Bonus:
 *  REQ13: - Animal can eat each other (Fossa)
 *
 * @author maxgls
 */
public class MainZooEmulator {

    public static void main(String[] args) {
        // Create ZooTimeEmulator with the specified amount of food on stock
        final Zoo zoo = new Zoo(700);
        // Add animals to cages
        zoo.addAnimal("beavers and squirrel",
                new Beaver(0),
                new Beaver(5),
                new Beaver(),
                new Squirrel());

        zoo.addAnimal("squirrels",
                new Squirrel(),
                new Squirrel(),
                new FlyingSquirrel(),
                new FlyingSquirrel());

        zoo.addAnimal("fossa and squirrel",
                new Fossa(),
                new Fossa(0),
                new Fossa(2),
                new Squirrel(),
                new FlyingSquirrel());
        // Start emulation for 7 days
        final ZooTimeEmulator zooTimeEmulator = new ZooTimeEmulator(zoo, new AnimalTimeBehavior(), new AcssTimeBehavior());
        // 7 - days (week), 9 AM - Start working hour, 19 / 7 PM - End working hour
        zooTimeEmulator.startEmulation(7, 9, 19);
    }
}
package com.j2core.maxgls.week4.zoo.utils;

/**
 * Represents the percent data type to work/limit with integer number in range [0,100]
 *
 * @author maxgls
 */
public final class Percent {

    public static final int MAX_VALUE = 100;
    public static final int MIN_VALUE = 0;

    private int value;

    public Percent() {
    }

    public Percent(int value) {
        this.value = getValueInRange(value);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = getValueInRange(value);
    }

    public void increase(int value) {
        this.value = getValueInRange(this.value + value);
    }

    public void decrease(int value) {
        this.value = getValueInRange(this.value - value);
    }

    private static int getValueInRange(int value) {
        return value >= MAX_VALUE ? MAX_VALUE :
                value < MIN_VALUE ? MIN_VALUE :
                        value;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}
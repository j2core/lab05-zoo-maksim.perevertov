package com.j2core.maxgls.week4.zoo.base;

import com.j2core.maxgls.week4.zoo.utils.Percent;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * Represents a base animal behavior that actions to do depending on the animal internal state and a cage parameters.
 *
 * @author maxgls
 */
public abstract class AnimalBehavior implements AnimalData {

    private static final Logger logger = Logger.getLogger(AnimalBehavior.class);

    private static final int FIND_FOOD_ENERGY_COST = 6;
    private static final int FIND_FOOD_FULLNESS_COST = 2;
    private static final int SLEEP_ENERGY_RECOVERY = 9;

    private final Random randChanceFindFood = new Random();
    protected final Random randMoveAction = new Random();

    /**
     * In cage an animal searches food depending on it personal percent
     * An animal has to find food before {@link #eat(Cage)}
     * REQ03 - MainZooEmulator
     *
     * @param cage the cage where food can be find
     * @return <tt>true</tt> if food has been found.
     */
    protected boolean findFood(Cage cage) {
        // Animal gets tired in search of food, search of food costs energy and fullness
        this.getEnergy().decrease(FIND_FOOD_ENERGY_COST);
        this.getFullness().decrease(FIND_FOOD_FULLNESS_COST);

        // Check food existence in the cage
        if (cage.getFoodCount() <= 0) {
            logger.info(this + " - [findFood] There is no food in the cage. fullnessPercent:" + this.getFullness() + ", energyPercent:" + this.getEnergy());
            return false;
        }

        // Animal probability/percent to find food
        final double randValue = this.randChanceFindFood.nextDouble();
        final double percent = this.getFindFoodPercent() / 100.0;
        boolean result = randValue <= percent;

        logger.info(String.format("%1$s - [findFood] The result is '%2$s' (random value:'%3$s', percent value:'%4$s')", this, result, randValue, percent));
        return result;
    }

    /**
     * An animal can eat if it's hungry
     * REQ03 - MainZooEmulator
     *
     * @param cage the cage where food can be eaten
     */
    protected void eat(Cage cage) {
        // Change animal fullness depending on amount of food in the cage
        final int reqFoodBeforeEating = this.getRequiredFoodCount();
        this.increaseFullness(cage.getFoodCount());
        final int reqFoodAfterEating = this.getRequiredFoodCount();

        // Decrease amount of food in the cage
        cage.removeFood(reqFoodBeforeEating - reqFoodAfterEating);
        logger.info(this + " - [eat] Amount of food in the cage:" + cage.getFoodCount() + ", animal fullness percent:" + this.getFullness());
    }

    /**
     * An animal can sleep if it's tired
     * REQ03 - MainZooEmulator
     */
    protected void sleep() {
        if (!this.isSleep()) {
            this.setSleep(true);
        }
        this.getEnergy().increase(SLEEP_ENERGY_RECOVERY);
        logger.info(this + " - [sleep] Energy percent:" + this.getEnergy());
    }

    /**
     * An animal do something when it's not hungry or does not sleep.
     * REQ03 - MainZooEmulator
     *
     * @param cage the cage where some action can be performed
     */
    protected abstract void doSomething(Cage cage);

    /**
     * An animal performs some action depending on internal state and context/cage (template method pattern).
     * This method calls other methods: {@link #sleep()}, {@link #findFood(Cage)}, {@link #eat(Cage)}}, {@link #doSomething(Cage)}
     * REQ04 - MainZooEmulator
     *
     * @param cage the context/environment where animal can perform actions
     */
    public void act(Cage cage) {
        if (cage == null) {
            throw new IllegalArgumentException("The parameter 'cage' is null");
        }

        // If animal is dead then exit
        if (this.isDead()) {
            logger.error(this + " - [act] Animal is dead");
            return;
        }

        // If animal sleeps then it can continue to sleep or wake up
        if (this.isSleep()) {
            if (this.getEnergy().getValue() < Percent.MAX_VALUE) {
                // Animal continues to sleep when energy has not been restored yet.
                this.sleep();
                return;
            }
            // Animal wakes up when energy is restored.
            this.setSleep(false);
        }

        // Animal starts to sleep if it gets tired.
        if (this.isTired()) {
            this.sleep();
            return;
        }

        // Animal finds food if it's hungry
        if (this.isHungry()) {
            final boolean isFoundFood = this.findFood(cage);
            if (isFoundFood) {
                this.eat(cage);
            }
            return;
        }

        // Animal does some action if it's not hungry or does not sleep
        this.doSomething(cage);
    }
}
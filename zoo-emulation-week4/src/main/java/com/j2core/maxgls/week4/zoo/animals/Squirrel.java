package com.j2core.maxgls.week4.zoo.animals;

import com.j2core.maxgls.week4.zoo.base.BaseAnimal;
import com.j2core.maxgls.week4.zoo.base.Cage;
import org.apache.log4j.Logger;

/**
 * Squirrel can bury food. If there are no food in cage, Squirrel can eat buried food.
 *
 * @author maxgls
 */
public class Squirrel extends BaseAnimal {

    private static final Logger logger = Logger.getLogger(Squirrel.class);

    private static final int NORMAL_FOOD_PORTION = 2;
    private static final int PERCENT_TO_FIND_FOOD = 80;

    private static final int JUMP_ENERGY_COST = 8;
    private static final int JUMP_FULLNESS_COST = 15;

    private int buriedFoodCounter;

    public Squirrel() {
        super(NORMAL_FOOD_PORTION, PERCENT_TO_FIND_FOOD);
    }

    protected Squirrel(int foodNormalPortion, int findFoodPercent) {
        super(foodNormalPortion, findFoodPercent);
    }

    @Override
    public void act(Cage cage) {
        super.act(cage);

        if (!this.isDead() &&
                this.isHungry() &&
                this.hasBuriedFood()) {
            this.eatBuriedFood();
        }
    }

    @Override
    protected void doSomething(Cage cage) {
        if (super.randMoveAction.nextBoolean()) {
            this.buryFood(cage);
        }
        else {
            this.jump();
        }
    }

    // Custom Squirrel's parameters (data):
    public boolean hasBuriedFood() {
        return this.buriedFoodCounter != 0;
    }

    // Custom Squirrel's actions:
    private void eatBuriedFood() {
        final int requiredFoodCount = this.getRequiredFoodCount();
        final int buriedFoodCount = this.buriedFoodCounter;
        this.increaseFullness(buriedFoodCount);
        // Decrease buried food counter depending on animal required amount of food
        this.buriedFoodCounter = this.buriedFoodCounter >= requiredFoodCount ?
                this.buriedFoodCounter - requiredFoodCount :
                0;
        logger.info(this + " - [eatBuriedFood] fullnessPercent:"+ this.getFullness() +", buriedFoodCount:"+ this.buriedFoodCounter);
    }

    private void buryFood(Cage cage) {
        if (!this.findFood(cage)) {
            logger.info(this + " - [buryFood] Food has not been found in the cage'");
            return;
        }
        // Squirrel prefers to bury amount of food = half of it's normal portion
        int requiredBuriedFoodCount = this.getFoodNormalPortion() / 2;
        // Minimum value is 1
        if (requiredBuriedFoodCount == 0) {
            requiredBuriedFoodCount = 1;
        }
        if (cage.getFoodCount() >= requiredBuriedFoodCount) {
            cage.removeFood(requiredBuriedFoodCount);
            this.buriedFoodCounter += requiredBuriedFoodCount;
        }
        logger.info(this + " - [buryFood] buriedFoodCounter:" + this.buriedFoodCounter + ", amount of food in cage:" + cage.getFoodCount());
    }

    private void jump() {
        this.getEnergy().decrease(JUMP_ENERGY_COST);
        this.getFullness().decrease(JUMP_FULLNESS_COST);
        logger.info(this + " - [jump] fullnessPercent:" + this.getFullness() + ", energyPercent:" + this.getEnergy());
    }
}